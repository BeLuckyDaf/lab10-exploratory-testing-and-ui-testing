# Lab10 - Explaratory testing and UI

## Homework

### Test 1 (implemented)

|       Test Tour       |           Reverso-Test 1          |
|:---------------------:|:---------------------------------:|
|        Guidance       |     Translate the word 'hello'    |
|        Duration       |             7 seconds             |
| Further Opportunities | Seeing diffrent uses for the word |

| # |            What was done            | Status | Comment |
|:-:|:-----------------------------------:|:------:|:-------:|
| 1 |           Find search bar           |   OK   |         |
| 2 |           Click search bar          |   OK   |         |
| 3 |             Type 'hello'            |   OK   |         |
| 4 | Find magnifying glass search button |   OK   |         |
| 5 |           Click the button          |   OK   |         |
| 6 | First world of the title is 'hello' |   OK   |         |
| 7 |       Search bar says 'hello'       |   OK   |         |

### Test 2

|       Test Tour       |               Reverso-Test 2              |
|:---------------------:|:-----------------------------------------:|
|        Guidance       |    Find synonyms for the word 'billing'   |
|        Duration       |                 10 seconds                |
| Further Opportunities | Seeing diffrent conjugations for the word |

| # |        What was done       | Status | Comment |
|:-:|:--------------------------:|:------:|:-------:|
| 1 |      Go to 'Synonyms'      |   OK   |         |
| 2 |       Find search bar      |   OK   |         |
| 3 |       Type 'Billing'       |   OK   |         |
| 4 |     Find search button     |   OK   |         |
| 5 |      Click the button      |   OK   |         |
| 6 | First synonym is 'invoice' |   OK   |         |

### Test 3

|       Test Tour       |               Reverso-Test 3              |
|:---------------------:|:-----------------------------------------:|
|        Guidance       |   Find synonyms for the word 'critisize'  |
|        Duration       |                 10 seconds                |
| Further Opportunities | Seeing diffrent conjugations for the verb |

| # |          What was done          | Status | Comment |
|:-:|:-------------------------------:|:------:|:-------:|
| 1 |       Go to 'Conjugation'       |   OK   |         |
| 2 |         Find search bar         |   OK   |         |
| 3 |         Type 'critisize'        |   OK   |         |
| 4 |        Find search button       |   OK   |         |
| 5 |         Click the button        |   OK   |         |
| 6 | Past participle is 'critisized' |   OK   |         |

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestReverso {

    @Test
    public void testReverso() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://context.reverso.net/translation/");
        WebElement findLine = driver.findElement(By.xpath("/html/body/div[3]/section[2]/div[1]/div[1]/input"));
        findLine.sendKeys("hello");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/div[3]/section[2]/div[1]/button"));
        searchButton.click();
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("hello", driver.getTitle().split(" ")[0]);
        findLine = driver.findElement(By.xpath("/html/body/div[3]/section[1]/div[2]/section[1]/div[1]/div[1]/input"));
        Assert.assertEquals("hello", findLine.getAttribute("value"));
        driver.quit();
    }
}
